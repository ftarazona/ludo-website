from django.shortcuts import render
from django.http import HttpResponse

#This view renders a page warning the site is not up yet
def notAvailable(request):
    return render(request, 'portal/index.html')
